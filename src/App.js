import React, { Component } from "react";
import "./App.css";
import * as firebase from "firebase";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      hasAccount: false,
      name: "",
      surname: "",
      key: "",
      value: "",
    };
  }
  componentDidMount() {
    const db = firebase.database();
    const name = db.ref("name");
    name.on("value", (elem) => {
      this.setState({ name: elem.val() });
    });
    // console.log(db);
  }

  handleChange = ({ target: { value, id } }) => {
    // console.log(value, id);
    this.setState({ [id]: value });
  };

  createAccount = () => {
    // // alert("Account has been created");
    const { email, password } = this.state;
    // // console.log(this.state);

    // //create new user
    // firebase
    //   .auth()
    //   .createUserWithEmailAndPassword(email, password)
    //   .catch((error) => console.error());

    //log in for existing user
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((response) => {
        this.setState({ hasAccount: true });
      })
      .catch((error) => console.error());
  };

  sendData = () => {
    const { key, value } = this.state;
    const db = firebase.database();
    db.ref(key).push(value);
    alert("your data has been writed");
  };

  getData = () => {
    const db = firebase.database();
    const surname = db.ref("surname");
    surname.on("value", (elem) => console.log(elem.val(), "test value"));
  };

  render() {
    // console.log(this.state);
    const { hasAccount, name } = this.state;
    this.getData();
    console.log(name);
    return (
      <div className="App">
        {hasAccount ? (
          <div>
            <div>You are signed in...</div>
            <input
              type="text"
              id="key"
              placeholder="input key"
              onChange={this.handleChange}
            />
            <input
              type="text"
              id="value"
              placeholder="input value"
              onChange={this.handleChange}
            />
            <button type="submit" onClick={this.sendData}>
              Submit
            </button>
          </div>
        ) : (
          <div className="login">
            <input
              type="text"
              id="email"
              placeholder="mail"
              onChange={this.handleChange}
            />
            <input
              type="password"
              id="password"
              placeholder="password"
              onChange={this.handleChange}
            />
            <button type="submit" onClick={this.createAccount}>
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}
