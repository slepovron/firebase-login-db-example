import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import * as firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyDTf8dl1fr67AS1IgNsiCfUlYH8puxbkW4",
  authDomain: "slepovron-site-login.firebaseapp.com",
  databaseURL: "https://slepovron-site-login.firebaseio.com",
  projectId: "slepovron-site-login",
  storageBucket: "slepovron-site-login.appspot.com",
  messagingSenderId: "1058778343935",
  appId: "1:1058778343935:web:dccc6a0842d990a150dfca",
};

firebase.initializeApp(firebaseConfig);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
